import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from IPython import display


class AnimateTrajectory:
    wingspan = 0.2
    height = 0.1
    color = (0.06666666666666667, 0.29411764705882354, 0.4627450980392157)
    drone_edges_body = np.array([
        [-wingspan, 0, 1],
        [wingspan, 0, 1],
        [0, 0, 1],
        [0, height, 1],
    ]).T

    def __init__(self, obstacle=None, is_jupyter=False):
        self.is_jupyter = is_jupyter
        self.fig, self.axes = plt.subplots(
            1, 2, tight_layout=True, gridspec_kw={'width_ratios': [3, 1]})

        def format_axes(axes):
            for ax in axes:
                ax.tick_params(labelbottom=False, labelleft=False, length=0)
                ax.set_anchor('N')
                ax.set_aspect("equal")
        format_axes(self.axes)

        self.base_line = []
        self.head_line = []
        drone_edges = self.get_drone_edges_world(np.zeros((6)))
        for ax in self.axes:
            self.base_line.append(ax.plot(
                drone_edges[0, [0, 1]],
                drone_edges[1, [0, 1]],
                color='k', marker='o', markerfacecolor=self.color, markersize=5,
            ))
            self.head_line.append(ax.plot(
                drone_edges[0, [2, 3]],
                drone_edges[1, [2, 3]],
                color=self.color, linewidth=2,
            ))
            
        if obstacle:
            self.axes[0].plot(obstacle[0], obstacle[1])
        
        plt.close()

    def get_drone_edges_world(self, state):
        phi = state[4]
        rotation_to_world = np.array([
            [np.cos(phi), np.sin(phi)],
            [-np.sin(phi), np.cos(phi)],
        ])

        # Transformation from body to world
        rotation_to_world_with_offset = np.hstack(
            (rotation_to_world, state[0:2, np.newaxis]))

        return rotation_to_world_with_offset @ self.drone_edges_body

    def render(self, state_trajectory, interval=20, save_gif=False):
        number_of_frames = state_trajectory.shape[1]
        select_every_x_frame = 2

        # Set limits for overall image
        bound_puffer = 1.1 * self.wingspan
        self.axes[0].set(
            xlim=(np.min(state_trajectory[0, :]) - bound_puffer,
                  np.max(state_trajectory[0, :]) + bound_puffer),
            ylim=(np.min(state_trajectory[1, :]) - bound_puffer,
                  np.max(state_trajectory[1, :]) + bound_puffer),
        )

        # Create animation
        def update_frame(frame_num):
            # Update drone state
            current_index = frame_num * select_every_x_frame
            state = state_trajectory[:, current_index]
            drone_edges = self.get_drone_edges_world(state)

            # Adjust limits of point of view subplot
            self.axes[1].set(
                xlim=(np.min(state[0]) - bound_puffer,
                      np.max(state[0]) + bound_puffer),
                ylim=(np.min(state[1]) - bound_puffer,
                      np.max(state[1]) + bound_puffer),
            )

            for line in self.base_line:
                line[0].set_data(drone_edges[0, [0, 1]],
                                 drone_edges[1, [0, 1]])
            for line in self.head_line:
                line[0].set_data(drone_edges[0, [2, 3]],
                                 drone_edges[1, [2, 3]])

        anim = FuncAnimation(
            self.fig, update_frame, frames=number_of_frames//select_every_x_frame, interval=interval)
        if save_gif: anim.save('drone_flight.gif', writer='imagemagick')
    
        # Show animation
        if not self.is_jupyter:
            plt.show()
        else:
            video = anim.to_html5_video()
            html = display.HTML(video)
            display.display(html)
