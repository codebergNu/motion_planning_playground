import casadi as cas
import numpy as np


def scale_variable(var, factor, direction):
    if direction == "to-scaled":
        if len(var.shape) == 1:
            out = var / factor
        else:
            out = var / factor[:, np.newaxis]
    elif direction == "to-si":
        if len(var.shape) == 1:
            out = var * factor
        else:
            out = var * factor[:, np.newaxis]
    else:
        raise("scale_variable requires a valid direction.")
    return out


def get_symbolic_polynomial(initial_state, terminal_state, terminal_time, order):
    # Define polynomial with symbolic argument and coefficients
    t = cas.MX.sym('t')
    coef = cas.MX.sym('coef', order + 1)
    poly = coef.T @ cas.vcat([cas.power(t, exp) for exp in range(order, -1, -1)])
    poly = cas.Function('polynomial', [t, coef], [poly])
    
    # Assemble linear equation system
    lgs_rhs = [initial_state, terminal_state]
    lgs_lhs = [poly(0, coef), poly(terminal_time, coef)]
    arg = terminal_time
    poly_dot = poly
    for ii in range(order - 1):
        if arg == terminal_time:
            poly_dot = cas.Function('polynomial', [t, coef], [cas.gradient(poly_dot(t, coef), t)])
            arg = 0
        else:
            arg = terminal_time
        lgs_rhs.append(0)
        lgs_lhs.append(poly_dot(arg, coef))
    
    # Extract coefficient matrix
    lgs_coef = cas.Function('lgs_coef', [t, coef], [cas.jacobian(cas.vcat(lgs_lhs), coef)])
    lgs_matrix = lgs_coef(0, np.zeros(order + 1)).full()
    
    # Solve for coefficients
    coef = np.linalg.solve(lgs_matrix, lgs_rhs)

    # Return symbolic polynomial function
    return cas.Function('polynomial_x', [t], [poly(t, coef)])


def erk4(dynamics, state, action, stepsize, internal_steps):
    stepsize = stepsize / internal_steps
    current_step = 0
    while current_step < internal_steps:
        k1 = dynamics(state, action)
        k2 = dynamics(state + stepsize / 2 * k1, action)
        k3 = dynamics(state + stepsize / 2 * k2, action)
        k4 = dynamics(state + stepsize * k3, action)
        state += stepsize / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
        current_step += 1
    return state
