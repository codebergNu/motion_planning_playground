# Motion Planning Playground
[![License: MPL 2.0](https://img.shields.io/badge/License-MPL_2.0-brightgreen.svg)](https://opensource.org/licenses/MPL-2.0)

First example is the computation of time-optimal trajectories for a planar drone model.
![](./flight_aba_obstacle_regularized.gif "flight_aba_obstacle_regularized")

Running __time_optimal_flight.ipynb__ requires [CasADi](https://web.casadi.org/) and [linear solver MA27](https://www.hsl.rl.ac.uk/ipopt/) to be available.



## Repository

https://codeberg.org/codebergNu/motion_planning_playground.git