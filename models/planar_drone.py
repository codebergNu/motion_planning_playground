import numpy as np
from dataclasses import dataclass


@dataclass
class Bounds:
    actions: np.ndarray = np.array([[0, 7.3], [0, 7.3]])
    action_rates: np.ndarray = np.array([[-7.3, 7.3], [-7.3, 7.3]]) / 1e-02


@dataclass
class Parameters:
    mass = 0.3 # Quadrocopter mass [kg]
    gravity = 9.81 # Earth acceleration [m*s^-2]
    arm_length = 0.26 # Distance to rotor from center point [m]
    inertia = 2.320e-04 # Inertia [kg*m^2]


class PlanarDrone:
    n_states = 6
    n_actions = 2
    state_labels = ['x', 'z', 'x_dot', 'z_dot', 'phi', 'phi_dot']
    state_units = ['m', 'm', 'm/s', 'm/s', 'rad', 'rad/s']
    action_labels = ['force_left', 'force_right']
    action_units = ['N', 'N']

    def __init__(self, integration_stepsize=0.001):
        self.integration_stepsize = integration_stepsize
        self.bounds = Bounds()
        self.parameters = Parameters()
        self.state = np.zeros(self.n_states)
        
    def reset_state(self):
        self.state = np.zeros(self.n_states)

    def dynamics(self, state, action):
        # Use aliases for states and actions
        x_dot = state[2]
        z_dot = state[3]
        phi = state[4]
        phi_dot = state[5]
        force_left = action[0]
        force_right = action[1]

        # Right handside of continuous system dynamics s_dot = f(s, a)
        state_derivative = (
            x_dot,
            z_dot,
            1 / self.parameters.mass * np.sin(phi) * (force_left + force_right),
            1 / self.parameters.mass * np.cos(phi) * (force_left + force_right) - self.parameters.gravity,
            phi_dot,
            1 / self.parameters.inertia * (force_left - force_right) * self.parameters.arm_length
        )
        return state_derivative

    def simulate_step(self, action):
        # Explicit Runge-Kutta 4th order integration scheme
        k1 = np.array(self.dynamics(self.state, action))
        k2 = np.array(self.dynamics(
            self.state + self.integration_stepsize / 2 * k1, action))
        k3 = np.array(self.dynamics(
            self.state + self.integration_stepsize / 2 * k2, action))
        k4 = np.array(self.dynamics(
            self.state + self.integration_stepsize * k3, action))
        self.state += self.integration_stepsize / \
            6 * (k1 + 2 * k2 + 2 * k3 + k4)
